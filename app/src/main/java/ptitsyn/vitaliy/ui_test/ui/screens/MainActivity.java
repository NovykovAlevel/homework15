package ptitsyn.vitaliy.ui_test.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import ptitsyn.vitaliy.ui_test.R;
import ptitsyn.vitaliy.ui_test.bl.arenas.BattleArena;
import ptitsyn.vitaliy.ui_test.bl.arenas.DuelArenaFactory;
import ptitsyn.vitaliy.ui_test.data.fighters.ArenaFighter;
import ptitsyn.vitaliy.ui_test.data.fighters.FighresFactory;
import ptitsyn.vitaliy.ui_test.data.fighters.FighterType;

public class MainActivity extends AppCompatActivity {

    private static final String CURRENT_FITER_KEY = "CURRENT_FITER_KEY";
    private static final String URI_VS_IMAGE = "https://www.superknockoffvs.com/img/SuperKnockoffVS_Icon.png";
    private float startHPFighter;
    private float startHPOpponent;
    private float startDamageFighter;
    private float startDamageOpponent;

    private ImageView profileImage;
    private TextView profileName;
    private TextView profileDescription;
    private TextView currentHp;
    private TextView currentArrmor;
    private TextView currentDamage;
    private TextView restartGame;
    private FloatingActionButton newFight;

    private LinearLayout battleHistory;
    private ImageView battleImageFighter1;
    private ImageView battleImageFighter2;
    private ImageView battleImageVS;
    private TextView battleNameFighter1;
    private TextView battleNameFighter2;
    private TextView battleHPFighter1;
    private TextView battleHPFighter2;
    private TextView battleDamageFighter1;
    private TextView battleDamageFighter2;
    private TextView battleResult;
    private View battleView;


    private ArenaFighter currentFighter;
    private ArenaFighter currentOpponent;
    private ArenaFighter currentWinner;

    private BattleArena arena;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initFighter(savedInstanceState);
        setDataOnUi();

    }

    private void loadingImage(ArenaFighter fighter, ImageView view) {
        loadingImage(fighter.getImageUrl(), view);
    }


    private void loadingImage(String uri, ImageView view) {
        Glide.with(this)
                .load(uri)
                .into(view);
    }

    private void setDataOnUi() {
        profileName.setText(currentFighter.getName());
        profileDescription.setText(currentFighter.getDescription());


        loadingImage(currentFighter, profileImage);

        currentHp.setText(String.valueOf(currentFighter.getHealth()));
        currentDamage.setText(String.valueOf(currentFighter.getDamage()));
        currentArrmor.setText(String.valueOf(currentFighter.getArmor()));


    }

    private void initFighter(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            currentFighter = FighresFactory.generateFighter(FighterType.DRAGON_RIDER, "Ikking");
        } else {
            currentFighter = savedInstanceState.getParcelable(CURRENT_FITER_KEY);
        }
    }

    private void initViews() {
        profileImage = findViewById(R.id.fighter_icon);

        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArrmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        restartGame = findViewById(R.id.btn_restart);
        restartGame.setOnClickListener(this::restart);

        newFight = findViewById(R.id.fab_btn);
        newFight.setOnClickListener(this::startNewFight);

        battleHistory = findViewById(R.id.battle_history);
    }

    private void restart(View view) {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CURRENT_FITER_KEY, currentFighter);
    }

    private void startNewFight(View view) {
        if (currentFighter.getHealth() > 0) {
            currentOpponent = FighresFactory.generateFighter(FighterType.randomType());
            startHPFighter = currentFighter.getHealth();
            startHPOpponent = currentOpponent.getHealth();
            startDamageFighter = currentFighter.getDamage();
            startDamageOpponent = currentOpponent.getDamage();

            arena = DuelArenaFactory.create(currentFighter, currentOpponent);
            arena.startBattle();
            currentWinner = arena.getWinner();
            addFighterResultOnLayout();
        } else {
            Toast.makeText(this, R.string.fighter_is_dead, Toast.LENGTH_SHORT).show();
        }

    }

    private void addFighterResultOnLayout() {
        initViewForInflate();
        fillingView();
        battleHistory.addView(battleView);
    }

    private void fillingView() {
        loadingImage(currentFighter, battleImageFighter1);
        loadingImage(currentOpponent, battleImageFighter2);
        loadingImage(URI_VS_IMAGE, battleImageVS);
        battleNameFighter1.setText(currentFighter.getName());
        battleNameFighter2.setText(currentOpponent.getName());
        battleHPFighter1.setText(String.format("HP: %s", String.valueOf(startHPFighter)));
        battleHPFighter2.setText(String.format("HP: %s",String.valueOf(startHPOpponent)));
        battleDamageFighter1.setText(String.format("Damage: %s",String.valueOf(startDamageFighter)));
        battleDamageFighter2.setText(String.format("Damage: %s",String.valueOf(startDamageOpponent)));
        battleResult.setText(String.format(
                getResources().getString(R.string.battle_result),
                currentWinner.getName(),
                currentWinner.getTotalDamage(),
                currentWinner.getHealth()));

    }

    private void initViewForInflate() {
        LayoutInflater inflater = getLayoutInflater();
        battleView = inflater.inflate(R.layout.battle_result, battleHistory, false);
        battleImageFighter1 = battleView.findViewById(R.id.fighter1);
        battleImageFighter2 = battleView.findViewById(R.id.fighter2);
        battleImageVS = battleView.findViewById(R.id.vs);
        battleNameFighter1 = battleView.findViewById(R.id.fighter1_name);
        battleNameFighter2 = battleView.findViewById(R.id.fighter2_name);
        battleHPFighter1 = battleView.findViewById(R.id.fighter1_hp);
        battleHPFighter2 = battleView.findViewById(R.id.fighter2_hp);
        battleDamageFighter1 = battleView.findViewById(R.id.fighter1_damage);
        battleDamageFighter2 = battleView.findViewById(R.id.fighter2_damage);
        battleResult = battleView.findViewById(R.id.battle_result);
    }


}
