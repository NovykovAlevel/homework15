package ptitsyn.vitaliy.ui_test.bl.arenas;

import ptitsyn.vitaliy.ui_test.data.fighters.ArenaFighter;

public class DuelArenaFactory {
    public static BattleArena create(ArenaFighter fighter1, ArenaFighter fighter2) {
        return new DuelArena(null, fighter1, fighter2, 20);
    }
}
