package ptitsyn.vitaliy.ui_test.bl.arenas;


import ptitsyn.vitaliy.ui_test.data.fighters.ArenaFighter;
import ptitsyn.vitaliy.ui_test.data.healers.Healer;

public class DuelArena extends BattleArena {
    protected ArenaFighter participant1;
    protected ArenaFighter participant2;
    private int roundCountMax;


    public DuelArena(Healer healer, ArenaFighter participant1, ArenaFighter participant2, int roundCountMax) {
        super(healer);
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.roundCountMax = roundCountMax;
        resetDamageCounter(participant2);
        resetDamageCounter(participant1);
    }

    private void resetDamageCounter(ArenaFighter participant) {
        participant.setTotalDamage(0);
    }

    @Override
    public void startBattle() {
        int currentRound = 0;
        while (currentRound < roundCountMax && isFightContinue(participant1, participant2)) {
            confrontation(participant1, participant2);
            currentRound++;
        }
    }

    @Override
    public ArenaFighter getWinner() {
        ArenaFighter winners = calculationOfWinner(participant1, participant2);
        if (winners != null) {
            return winners;
        } else {
            return null;
        }
    }


}
