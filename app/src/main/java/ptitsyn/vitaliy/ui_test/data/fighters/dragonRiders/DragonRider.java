package ptitsyn.vitaliy.ui_test.data.fighters.dragonRiders;


import android.os.Parcel;

import ptitsyn.vitaliy.ui_test.data.fighters.ArenaFighter;
import ptitsyn.vitaliy.ui_test.data.fighters.FighterType;
import ptitsyn.vitaliy.ui_test.data.fighters.dragons.Dragon;

public class DragonRider extends ArenaFighter {
    private Dragon ridingDragon;
    private boolean flagDragon;

    public DragonRider(String name, float health, float damage, float armor, String url) {
        super(name, health, damage, armor, url);
        classFighter = FighterType.DRAGON_RIDER.name();
    }

    public DragonRider(Parcel in) {
        super(in);
        this.ridingDragon = in.readParcelable(Dragon.class.getClassLoader());
    }

    @Override
    public String getDescription() {
        return "Description: \n" +
                "Mighty fighter with medium attack and little stamina. \n" +
                "Special skill: can saddle an enemy dragon, appropriating the rest of his life.";
    }

    @Override
    public float attack (ArenaFighter var1) {
        if( var1 instanceof Dragon ) {
            attackDragon((Dragon) var1);
            return 0;
        } else {
            return var1.damaged(this.damage);
        }
    }

    private void attackDragon (Dragon dragon) {
        flagDragon = true;
        this.ridingDragon = dragon;
        health += dragon.getHealth();
        dragon.setHealth(0);
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(ridingDragon,flags);
    }

    @Override
    protected int getType() {
        return FighterType.DRAGON_RIDER.getType();
    }
}
