package ptitsyn.vitaliy.ui_test.data.fighters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import ptitsyn.vitaliy.ui_test.data.fighters.dragonRiders.DragonRider;
import ptitsyn.vitaliy.ui_test.data.fighters.dragons.Dragon;
import ptitsyn.vitaliy.ui_test.data.fighters.knights.Knight;

public class FighresFactory {
    static private int counterNames = 0;
    static HashMap<FighterType, String> urlDataBase = new HashMap<>();
    static List<String> listNames = new ArrayList<>();



    static {
        urlDataBase.put(FighterType.DRAGON, "https://dragon-quest.org/images/thumb/4/4c/DQIII_Wyrtle.png/280px-DQIII_Wyrtle.png");
        urlDataBase.put(FighterType.DRAGON_RIDER, "https://i.pinimg.com/originals/59/5e/c6/595ec61c7774c73ded78f8e89d353fa6.png");
        urlDataBase.put(FighterType.KNIGHT, "https://dragon-quest.org/images/thumb/b/b4/DQV_Slime_Knight.png/250px-DQV_Slime_Knight.png");

        listNames.add("Lucifer");
        listNames.add("Neo");
        listNames.add("Clark");
        listNames.add("Jon");
        listNames.add("Aria");
        listNames.add("Harry");
        listNames.add("Ron");
        listNames.add("Kokosik");
    }




    static class UnknownFighterException extends RuntimeException {
    }

    public static ArenaFighter generateFighter (FighterType fighterType) {
        return generateFighter(fighterType, generateName());
    }

    public static ArenaFighter generateFighter (FighterType fighterType, String name) {
        switch ( fighterType ) {
            case DRAGON:
                return new Dragon(name, generatevalue(300),
                        generatevalue(30),
                        generateProcentVlaue(), 0,
                        urlDataBase.get(fighterType));
            case KNIGHT:
                return new Knight(name, generatevalue(300),
                        generatevalue(30),
                        generateProcentVlaue(),
                        generateProcentVlaue()/2,
                        urlDataBase.get(fighterType)
                );
            case DRAGON_RIDER:
                return new DragonRider(name,
                        generatevalue(300),
                        generatevalue(30),
                        generateProcentVlaue(),
                        urlDataBase.get(fighterType));

        }
        throw new UnknownFighterException();
    }

    private static float generatevalue (int max) {
        Random random = new Random();
        return random.nextInt(max)+30;

    }

    private static float generateProcentVlaue () {
        Random random = new Random();
        return random.nextFloat()*30;

    }

    private static String generateName() {
        String name;
        if (counterNames < listNames.size()) {
            name = listNames.get(counterNames);
        } else {
            counterNames = 0;
            name = listNames.get(counterNames);
        }
        counterNames++;
        return name;
    }

}
